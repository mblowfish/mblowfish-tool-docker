FROM node:8

MAINTAINER MBlowfish <mostafa.barmshory@gmail.com>

# Update to the new version and install tools
RUN apt-get update
RUN apt-get -yq install chromium git
RUN npm install -g bower grunt-cli

# add builder user
RUN useradd -m -p 123 builder